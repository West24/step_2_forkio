# Step_2_forkio

Developer: Vitalii Bocharov 

Group: fe_31

List of technologies used: HTML, CSS, JavaScript,
 
Gulp and its packages:

 "browser-sync": "^2.27.5",
    "del": "^6.0.0",
    "gulp": "^4.0.2",
    "gulp-autoprefixer": "^8.0.0",
    "gulp-clean-css": "^4.3.0",
    "gulp-concat": "^2.6.1",
    "gulp-file-include": "^2.3.0",
    "gulp-image": "^6.2.1",
    "gulp-newer": "^1.4.0",
    "gulp-sass": "^5.0.0",
    "gulp-sourcemaps": "^3.0.0",
    "gulp-uglify-es": "^3.0.0",
    "i": "^0.3.6",
    "node-sass": "^2.1.1",
    "npm": "^7.20.6",
    "sass": "^1.37.5"

Created in WebStorm