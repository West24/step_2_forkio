"use strict";
const burger = document.querySelector(".hamburger");
const menu = document.querySelector(".header-page__menu");
const burgerLine = Array.from(document.querySelectorAll(".hamburger__line"));



burger.addEventListener("click", function (e) {
   e.stopPropagation();
   menu.classList.toggle("active");
   burgerLine.forEach(element => {
      element.classList.toggle("change");
   });

});

document.onclick = function (event) {

   console.log(event.target);
   if (event.target.className != "menu__link") {
      menu.classList.remove("active");
      burgerLine.forEach(element => {
         element.classList.remove("change");
      });
   }
};