"use strict";
const {
   src,
   dest,
   parallel,
   series,
   watch
} = require('gulp');
const browserSync = require('browser-sync').create();
const concat = require('gulp-concat');
const sass = require('gulp-sass')(require('sass'));
const uglify = require('gulp-uglify-es').default;
const autoprefixer = require('gulp-autoprefixer');
const cleancss = require('gulp-clean-css');
const imagemin = require('gulp-image');
const newer = require('gulp-newer');
const del = require('del');
const sourcemaps = require('gulp-sourcemaps');
const include = require('gulp-file-include');


const browsersync = () => {
   browserSync.init({
      server: {
         baseDir: "./",
      },
      notify: false
   });
};


const images = () => {
   return src("./src/img/**/*")
      .pipe(newer("./dist/img"))
      .pipe(imagemin())
      .pipe(dest("./dist/img"))

};

const fileinclude = () => {
   return src("./src/html/index.html")
       .pipe(include({
          prefix: '@@',
          basepath: '@file'
       }))
       .pipe(concat('index.html'))
       .pipe(dest('./'))
       .pipe(browserSync.reload({
          stream: true
       }))
};
const scripts = () => {
   return src("./src/js/**/*.js")
      .pipe(concat("scripts.min.js"))
      .pipe(uglify())
      .pipe(dest("./dist/scripts"))
      .pipe(browserSync.reload({
         stream: true
      }))

};

const startWatch = () => {
   watch("./src/html/**/*.html", parallel(fileinclude));
   watch("./src/scss/**/*.scss", parallel(styles));
   watch("./src/js/**/*.js", parallel(scripts));
   watch("./src/img/**/*.{gif,jpg,png,svg}", parallel(images));
   watch("./*.html").on("change", browserSync.reload);
};



const styles = () => {
   return src("./src/scss/*.scss")
      .pipe(sourcemaps.init())
      .pipe(sass().on('error', sass.logError))
      .pipe(sourcemaps.write())
      .pipe(autoprefixer({
         overrideBrowserslist: ["last 10 versions"],
         grid: true
      }))
      .pipe(concat("styles.min.css"))
      .pipe(cleancss({
         level: {
            2: {
               specialComments: 0
            }
         }
      }))
      .pipe(dest('./dist/css'))
      .pipe(browserSync.stream())
};

const clean = () => {
   return del('./dist/**/*');
};

exports.browsersync = browsersync;
exports.scripts = scripts;
exports.startWatch = startWatch;
exports.styles = styles;
exports.images = images;
exports.clean = clean;
exports.fileinclude = fileinclude;


exports.build = series(clean, scripts, styles, images);
exports.dev = parallel(browsersync, startWatch, series(clean, scripts, styles, images, fileinclude));